import datetime
import unittest
import unittest.mock

import history


def create_show_name(collection: str) -> str:
    return "Show Name: {collection}: Episode Name".format(**{"collection": collection})


class HistoryTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.file_data = [
            {"title": "Title", "date": "Date"},
            {"title": "TV Show 1", "date": "1/1/16"},
            {"title": "TV Show 2", "date": "2/1/16"},
            {"title": "TV Show 3", "date": "12/31/18"},
            {"title": "Movie 1", "date": "12/31/19"},
        ]

    def test_get_series_collection(self):
        self.assertEqual("Show Name", history.get_series(create_show_name("Collection")))

    def test_get_series_year(self):
        self.assertEqual("Show Name", history.get_series(create_show_name("The Seventeenth Year")))

    def test_get_series_season(self):
        self.assertEqual("Show Name", history.get_series(create_show_name("Season 5")))

    def test_get_series_part(self):
        self.assertEqual("Show Name", history.get_series(create_show_name("Part 1")))

    def test_get_series_chapter(self):
        self.assertEqual("Show Name", history.get_series(create_show_name("Chapter 3")))

    def test_get_series_no_middle_section(self):
        self.assertEqual("Show Name", history.get_series("Show Name: Episode Name"))

    def test_get_datetime(self):
        self.assertEqual(datetime.datetime(2010, 10, 1), history.get_datetime("10/1/10"))

    def test_get_datetime_bad_format(self):
        self.assertIsNone(history.get_datetime("date"))

    def test_gen_viewing_history(self):
        with unittest.mock.patch("csv.DictReader", return_value=self.file_data):
            i = 0
            for i, row in enumerate(history.gen_viewing_history(), start=1):
                self.assertSetEqual({"title", "date", "series"}, set(row))
            self.assertEqual(4, i)

    def test_get_most_recent_date(self):
        file_data = [
            {"date": datetime.datetime(2016, 1, 1)},
            {"date": datetime.datetime(2016, 2, 1)},
            {"date": datetime.datetime(2018, 12, 31)},
            {"date": datetime.datetime(2019, 12, 31)},
        ]
        self.assertEqual(datetime.datetime(2019, 12, 31), history.get_most_recent_date(file_data))

    def test_get_earliest_date(self):
        file_data = [
            {"date": datetime.datetime(2016, 1, 1)},
            {"date": datetime.datetime(2016, 2, 1)},
            {"date": datetime.datetime(2018, 12, 31)},
            {"date": datetime.datetime(2019, 12, 31)},
        ]
        self.assertEqual(datetime.datetime(2016, 1, 1), history.get_earliest_date(file_data))

    def test_display_number_of_items_watched(self):
        self.assertIsInstance(history.display_number_of_items_watched(5), str)

    def test_display_yearly_viewing_habits(self):
        self.assertEqual("5 show episodes/movies watched per year.", history.display_yearly_viewing_habits(5, 365))

    def test_display_monthly_viewing_habits(self):
        self.assertEqual("1 show episodes/movies watched per month.", history.display_monthly_viewing_habits(12, 365))

    def test_display_weekly_viewing_habits(self):
        self.assertEqual("1 show episodes/movies watched per week.", history.display_weekly_viewing_habits(52, 365))

    def test_display_daily_viewing_habits(self):
        self.assertEqual("1.0 show episodes/movies watched per day.", history.display_daily_viewing_habits(365, 365))

    def test_display_number_of_series_and_movies_watched(self):
        self.assertEqual("365 different shows / movies watched overall.",
                         history.display_number_of_series_and_movies_watched(365))
