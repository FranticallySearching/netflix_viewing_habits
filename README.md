#### Steps to use project
1. Clone the repo
2. Download a copy of your netflix watch history [netflix.com/viewingactivity](https://www.netflix.com/viewingactivity)
3. Move the downloaded file into the root of the project folder. It should look similar to the NetflixViewingHistoryTemplate.csv file.
4. In the project root, run the following command: `python history.py`
