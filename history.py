"""
Notes:
1) The NetflixViewingHistory.csv file only contains the most recent viewing.
2) Any re-watched tv show, series, or movie will not shown twice.
3) The stats presented will not take into account if a person re-watches the same shows or movies.
"""

import csv
from datetime import datetime


def explain():
    """ Gather all of the watched data and display the Netflix viewing habits """
    watched = list(gen_viewing_history())

    number_of_items_watched = len(watched)
    display_number_of_items_watched(number_of_items_watched)
    number_of_days = (get_most_recent_date(watched) - get_earliest_date(watched)).days
    print(display_yearly_viewing_habits(number_of_items_watched, number_of_days))
    print(display_monthly_viewing_habits(number_of_items_watched, number_of_days))
    print(display_weekly_viewing_habits(number_of_items_watched, number_of_days))
    print(display_daily_viewing_habits(number_of_items_watched, number_of_days))
    print(display_number_of_series_and_movies_watched(len({watched_item["series"] for watched_item in watched})))


def get_series(title: str) -> str:
    """ :return str of the series name from the full title """
    return title.split(":")[0]


def get_datetime(date: str) -> (datetime, None):
    """ :return datetime representation of string. If given wrong format, return None """
    try:
        return datetime.strptime(date, "%m/%d/%y")
    except ValueError:
        return None


def gen_viewing_history(file_name: str = "NetflixViewingHistory.csv") -> dict:
    """ :return generator for returning dictionaries of title, date, and series name """
    with open(file_name, encoding="UTF-8") as f:
        for row in csv.DictReader(f, fieldnames=["title", "date"]):
            row["date"] = get_datetime(row["date"])
            if not row["date"]:
                continue
            row["series"] = get_series(row["title"])
            yield row


def get_most_recent_date(rows: iter) -> datetime:
    """ :return datetime of the most recent date found """
    most_recent = None
    for row in rows:
        if most_recent is None:
            most_recent = row["date"]
            continue
        most_recent = max(most_recent, row["date"])
    return most_recent


def get_earliest_date(rows: iter) -> datetime:
    """ :return datetime of the earliest date found """
    earliest = None
    for row in rows:
        if earliest is None:
            earliest = row["date"]
            continue
        earliest = min(earliest, row["date"])
    return earliest


def display_number_of_items_watched(number_of_items_watched: int) -> str:
    """ :return str of the number of items watched """
    return "{number} items watched.".format(**{"number": number_of_items_watched})


def display_yearly_viewing_habits(number_of_items_watched: int, number_of_days_watching: int) -> str:
    """ :return str of the number of show episodes / movies are watched per year """
    number_of_yearly_watching = number_of_days_watching / 365
    return "{watched} show episodes/movies watched per year.".format(
        **{"watched": int(round(number_of_items_watched / max(number_of_yearly_watching, 1), 0))})


def display_monthly_viewing_habits(number_of_items_watched: int, number_of_days_watching: int) -> str:
    """ :return str of the number of show episodes / movies are watched per month """
    number_of_months_watching = number_of_days_watching / 30
    return "{watched} show episodes/movies watched per month.".format(
        **{"watched": int(round(number_of_items_watched / max(number_of_months_watching, 1), 0))})


def display_weekly_viewing_habits(number_of_items_watched: int, number_of_days_watching: int) -> str:
    """ :return str of the number of show episodes / movies are watched per week """
    number_of_weeks_watching = number_of_days_watching / 7
    return "{watched} show episodes/movies watched per week.".format(
        **{"watched": int(round(number_of_items_watched / max(number_of_weeks_watching, 1), 0))})


def display_daily_viewing_habits(number_of_items_watched: int, number_of_days_watching: int) -> str:
    """ :return str of the number of show episodes / movies are watched per day """
    return "{watched} show episodes/movies watched per day.".format(
        **{"watched": round(number_of_items_watched / max(number_of_days_watching, 1), 2)})


def display_number_of_series_and_movies_watched(number_of_items_watched: int) -> str:
    """ :return str of the number of unique shows / movies watched overall """
    return "{watched} different shows / movies watched overall.".format(**{"watched": number_of_items_watched})


if __name__ == "__main__":
    explain()
